import Vue from 'vue'
import Router from 'vue-router'
import Control from '../components/Control.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Control',
            component: Control
        }
    ]
})
